var mongoose = require('mongoose');

var NItemSchema = {
    name:String,
    purchased:Boolean,
    id:String
};

var NItem = mongoose.model('NItem',NItemSchema,"nitems");

module.exports = NItem;
