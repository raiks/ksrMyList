module.exports = function (app){

    var NItem = require('./../models/NItem.js');

    app.route('/api/items')
    .get(function(req,res){
        NItem.find(function(error,doc){
            res.send(doc);
        })

    })
    .post(function(req,res){
        console.log("Adding item...",item);
        var item = req.body;
      //  items.push(item);
        var nItem = new NItem(item);
        nItem.save(function(err,data){
            res.status(300).send();
        })
    });

    app.route('/api/items/:id')
    .delete(function(req,res){
        console.log("removing...",req.params.id);
        NItem.findOne({
            _id:req.params.id
        }).remove(function(x){
            console.log("removed.",x);
        });
    })
    .patch(function(req,res){
        NItem.findOne({
            _id:req.body._id
        },function(error,doc){
            console.log(error);
            for (var key in req.body){
                doc[key] = req.body[key];
            }
            doc.save();
            res.status(200).send();
        })
    })

}
