var express = require('express');
var app = new express();

var parser = require('body-parser');

require('./database.js');

var port = process.env.PORT || 7777;

app.get('/', function(req, res){
    console.log("Hello from DEEP");
    res.render('./../app/index.ejs',{});
})
.use(express.static(__dirname + '/../.tmp'))
.listen(port);

app.use(parser.json());
app.use(parser.urlencoded({extended:false}));

require('./routes/items.js')(app);
