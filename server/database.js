var mongoose = require('mongoose');
var NItem = require('./models/NItem.js');

mongoose.connect('mongodb://localhost/item',function(){
    console.log("connected.");

    mongoose.connection.db.dropDatabase();

    var items = [{
        name:"Mars"
    },{
        name:"Twirl"
    },{
        name:"Lion Bar",
        purchased:true
    },{
        name:"Bounty"
    }];

    items.forEach(function(item){
        new NItem(item).save();
    })
})
