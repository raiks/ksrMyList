var dispatcher = require('./../dispatcher.js');

module.exports = {
    add:function(item){
        dispatcher.dispatch({
            payload:item,
            type:"n-item:add"
        })
    },
    delete:function(item){
        dispatcher.dispatch({
            payload:item,
            type:"n-item:delete"
        })
    },
    buy:function(item){
        dispatcher.dispatch({
            payload:item,
            type:"n-item:buy"
        })
    },
    unbuy:function(item){
        dispatcher.dispatch({
            payload:item,
            type:"n-item:unbuy"
        })
    }
}
