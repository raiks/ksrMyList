var React = require('react/addons');
var Item = require('./Item.jsx');
var ListAddItem = require('./ListAddItem.jsx');

module.exports = React.createClass({
    render:function(){
        return (
            <div>
                <h1>Shopping List </h1>
                <div>
                    {this.props.items.map(function(item,index){
                        return (
                            <Item item={item} key={"item"+index}/>
                        )
                    })
                    }
                </div>
                <ListAddItem />
            </div>
        )
    }
})
