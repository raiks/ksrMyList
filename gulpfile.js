var gulp = require('gulp');
var LiveServer = require('gulp-live-server');
var browserSync = require('browser-sync');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var reactify = require('reactify');

var gulp   = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var notify = require('gulp-notify');
var rename = require('gulp-rename');
var wrap   = require('gulp-wrap');
var nib    = require('nib');
var stylus = require('gulp-stylus');
var build = require('gulp-build');

gulp.task('build', function() {
  gulp.src('scripts/*.js')
      .pipe(build({ GA_ID: '123456' }))
      .pipe(gulp.dest('dist'))
});

gulp.task('live-server',function(){
    var server = new LiveServer('server/main.js');
    server.start();
})

gulp.task('bundle',['copy'],function(){
    return browserify({
        entries:'app/main.jsx',
        debug:true,
    })
    .transform(reactify)
    .bundle()
    .pipe(source('app.js'))
    .pipe(gulp.dest('./.tmp'));
})

gulp.task('copy',function(){
    gulp.src(['app/*.css'])
    .pipe(gulp.dest('./.tmp'));
})

gulp.task('serve',['bundle','live-server'],function(){
    browserSync.init(null,{
        proxy:"http://localhost:7777",
        port: 9001
    })
})

gulp.task('javascript', function() {
  return gulp.src('assets/javascript/*.js')
    .pipe(wrap('(function($, window){<%= contents %>}(jQuery, window));'))
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(uglify())
    .pipe(gulp.dest('public/javascripts'))
    .pipe(notify({ message: 'Scripts task complete' }));
});

gulp.task('css', function() {
  return gulp.src('assets/style/*.styl')
    .pipe(stylus({
      use: nib(),
      compress: true
    }))
    .pipe(gulp.dest('public/stylesheets'))
    .pipe(notify({ message: 'CSS task complete' }));
});

/*gulp.task('build', function(){
  gulp.start('javascript');
	gulp.start('css');
});
*/
/* gulp.task('default',['serve']); */
gulp.task('default',['serve'],function() {
  gulp.start('javascript');
	gulp.start('css');
});
